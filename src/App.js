import React, { Component } from 'react';
import './App.css';
import FormTab from "./components/FormTab";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Postchain Certificate Tool Demo</h1>
        </header>
        <FormTab />
      </div>
    );
  }
}

export default App;
