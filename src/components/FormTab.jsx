import React, { Component } from 'react';
import '../App.css';
import { Tab, Tabs } from 'react-bootstrap';
import SubmitForm from "./SubmitForm";
import SearchForm from "./SearchForm";

class FormTab extends Component {
    render() {
        return (
            <div className="container centerDiv">
                <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                    <Tab eventKey={1} title="Submit">
                        <SubmitForm />
                    </Tab>
                    <Tab eventKey={2} title="Search">
                        <SearchForm />
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default FormTab;