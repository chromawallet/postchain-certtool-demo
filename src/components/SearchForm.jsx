import React, { Component } from 'react';
import '../App.css';
import { restClient } from 'postchain-client';

const id = "19800316-7653";
const authority = "0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15";
const url = "http://foo.esplix-postchain-workspace.chromaway.net:5002";
const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";

class SearchForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: id,
            authority: authority,
            url: url,
            blockchainRID: blockchainRID
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setResultText = this.setResultText.bind(this);
    }

    setResultText(text) {
        var resultDisplayArea = document.getElementById('resultDisplayAreaSearch');
        resultDisplayArea.innerText = text;
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
        console.log(event.target.value);
    }

    handleSubmit(event) {
        event.preventDefault();
        // var updatedID = this.state.id;
        // var updatedAuthority = this.state.authority;
        // var updatedURL = this.state.url;
        const myRestClient = restClient.createRestClient(this.state.url);
        // const myGtxClient = gtxClient.createClient(myRestClient, Buffer.from(blockchainRID, 'hex'), ["certificate"]);
        console.log(`Updated ID = ${this.state.id}, updated authority = ${this.state.authority}`)

        myRestClient.query({type:'get_certificates', id: this.state.id , authority: this.state.authority},
            (err, res) => {
                if (err) {
                    this.setResultText(err.message);
                }
                else {
                    this.setResultText(JSON.stringify(res));
                }
                console.log(err, res);
            });
    }

    render() {
        return (
            <div className="container centerDiv">
                <h1 className="centerText">Search Certificate</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Civic registration number:
                        <input type="text" id="id" defaultValue={this.state.id} onChange={this.handleChange} />
                    </label>
                    <label>
                        Authority:
                        <input type="text" id="authority" defaultValue={this.state.authority} onChange={this.handleChange} />
                    </label>
                    <label>
                        URL:
                        <input type="url" id="url" defaultValue={this.state.url} onChange={this.handleChange} />
                    </label>
                    <label>
                        Blockchain RID:
                        <input type="text" id="blockchainRID" defaultValue={this.state.blockchainRID} onChange={this.handleChange} />
                    </label>
                    <label>
                        <input type="submit" value="Search" />
                    </label>
                </form>
                <label>
                    Result:
                    <pre id="resultDisplayAreaSearch"/>
                </label>
            </div>
        );
    }
}

export default SearchForm;