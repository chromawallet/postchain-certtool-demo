import React, { Component } from 'react';
import '../App.css';
import { restClient, gtxClient } from 'postchain-client';

const id = "19800316-7653";
const name = "Alice Brown"
const pubkey = "0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15";
const expires = Math.floor(Date.now()) + 60*60*24*365;
const reason = "";
const auPubKey = "0373599a61cc6b3bc02a78c34313e1737ae9cfd56b9bb24360b437d469efdf3b15";
const auPrivKey = "a68957ba735f98f8d8169ee54fccf2ccf193d97d95e46342bb5e05007c51f324";
const url = "http://foo.esplix-postchain-workspace.chromaway.net:5002";
const blockchainRID = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";

class SubmitForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: id,
            name: name,
            pubkey: pubkey,
            expires: expires,
            reason: reason,
            auPubKey: auPubKey,
            auPrivKey: auPrivKey,
            url: url,
            blockchainRID: blockchainRID
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setResultText = this.setResultText.bind(this);
    }

    setResultText(text) {
        var resultDisplayArea = document.getElementById('resultDisplayAreaSubmit');
        resultDisplayArea.innerText = text;
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const myRestClient = restClient.createRestClient(this.state.url);
        const myGtxClient = gtxClient.createClient(myRestClient, Buffer.from(this.state.blockchainRID, 'hex'), ["certificate"]);
        const rq = myGtxClient.newRequest([Buffer.from(this.state.auPubKey, 'hex')]);
        rq.certificate(this.state.id, this.state.name, Buffer.from(this.state.pubkey, 'hex'), this.state.expires, Buffer.from(this.state.auPubKey, 'hex'), Buffer.from(this.state.reason, 'hex'));
        console.log(rq);
        rq.sign(Buffer.from(this.state.auPrivKey, 'hex'));
        rq.send( (err, res) => {
            if (err) {
                this.setResultText(err.message);
            }
            else {
                this.setResultText("Success!");
            }
            console.log(err, res);
        });
    }

    render() {
        return (
            <div className="container centerDiv">
                <h1 className="centerText">Submit Certificate</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Civic registration number (personnummer):
                        <input type="text" id="id" defaultValue={this.state.id} onChange={this.handleChange} />
                    </label>
                    <label>
                        Name:
                        <input type="text" id="name" defaultValue={this.state.name} onChange={this.handleChange} />
                    </label>
                    <label>
                        Esplix pubkey:
                        <input type="text" id="pubkey" defaultValue={this.state.pubkey} onChange={this.handleChange} />
                    </label>
                    <label>
                        Reason (Base64 encoded certificate proving the relationship between name, civic registration number &amp; Esplix pubkey):
                        <textarea id="reasonDisplayArea"></textarea>
                    </label>
                    <label>
                        Authority Pubkey (CA authority, if this app is used directly by CA, otherwise Proxy CA or self-signed):
                        <input type="text" id="auPubKey" defaultValue={this.state.auPubKey} onChange={this.handleChange} />
                    </label>
                    <label>
                        Authority Privkey (CA authority, if this app is used directly by CA, otherwise Proxy CA or self-signed):
                        <input type="text" id="auPrivKey" defaultValue={this.state.auPrivKey} onChange={this.handleChange} />
                    </label>
                    <label>
                        URL of API port of postchain to register this directory entry:
                        <input type="url" id="url" defaultValue={this.state.url} onChange={this.handleChange} />
                    </label>
                    <label>
                        Blockchain RID (which blockchain to post to, since a Postchain node may serve more than one blockchain):
                        <input type="text" id="blockchainRID" defaultValue={this.state.blockchainRID} onChange={this.handleChange} />
                    </label>
                    <label>
                        <input type="submit" value="Register" />
                    </label>
                </form>
                <label>
                    Result:
                    <pre id="resultDisplayAreaSubmit"/>
                </label>
            </div>
        );
    }
}

export default SubmitForm;