import * as express from 'express';
import * as cors from 'cors';
import { AuthTeliaID } from './telia-id.auth';

const app = express();

const teliaid = new AuthTeliaID();

app.use(express.urlencoded());
app.use(cors());

app.post("/teliaid/sign", (req, res, next) => {
    if ('personNumber' in req.body) {
        teliaid.sign(req.body.personNumber, req.body.vdata,  req.body.nvdata, (result: any, err: any) => {
            if (err) {
                res.status(500).send("Error");
            } else {
                console.log(result);
                res.send(result.SignResult.orderRef);
            }
        });
    } else {
        res.status(400).send("personNumber required");
    }
});

app.post("/teliaid/collect", (req, res, next) => {
  if ('order' in req.body) {
    teliaid.collect(req.body.order, 1, (result: any, err: any) => {
      if (err) {
	res.status(500).send("error");
      } else {
	    console.log(result);
	    if (result.CollectResult.progressStatus === 'COMPLETE')
	        res.send(JSON.stringify({
                signature: result.CollectResult.signature,
                userInfo: result.CollectResult.userInfo
            }));
        else {
	        res.send("");
        }
      }
    });
  } else next();
});

app.listen(5127, () => console.log('Example app listening on port 5127!'))
