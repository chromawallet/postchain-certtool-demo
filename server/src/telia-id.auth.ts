import * as soap from "soap";
import * as fs from "fs";

const logger = {
 info: (msg: string) => console.log(msg),
 error: (msg: string, err: Error) => console.log("ERROR", msg, err.stack || err.toString())
};


const pfxPath = "./forlitande.p12";
const pfxPass = "GQ3s3FmE";
const bankIdUrl = "./TeliaEID Relying Party Service API 1.0 WSDL.wsdl";

/**
 * TeliaID integration.
 *
 * More info about Telia services:
 *
 * https://eid.trust.telia.com
 * https://eid.trust.telia.com/Portals/9/Doc/RP/TeliaEID%20Relying%20Party%20Service%20API%201.0%20Reference.pdf
 */
export class AuthTeliaID {
    soapClient: any;
    constructor() {
        this.init(function () {
            logger.info("Telia - soap client initialised");
        });
    }
    /**
     * Function generate SOAP client and should be called before Express server starts
     */
    init(callback: any) {
        const self = this;

        const pfxFile = fs.readFileSync(pfxPath);

        soap.createClient(bankIdUrl, {
            wsdl_options: {
                rejectUnauthorized: true,
                strictSSL: false,
                pfx: pfxFile,
                passphrase: pfxPass
            }
        }, function (err: any, client: soap.Client) {

            logger.info("Telia client inited...");

            self.soapClient = client;

            // SSL config not for production!!!!
            self.soapClient.setSecurity(new soap.ClientSSLSecurityPFX(
                fs.readFileSync(pfxPath),
                pfxPass,
                {   /*default request options like */
                    strictSSL: false,
                    rejectUnauthorized: false,
                    // hostname: 'some-hostname'
                    // secureOptions: constants.SSL_OP_NO_TLSv1_2,
                    // forever: true,
                }
            ));

            if (err) {
                logger.error("Error:", err);
            } else {
                callback();
            }
        }
        );
    }
    /**
     * Authenticate client if function recived the personalNumber
     * The request will be sent to user Mobile device to confirm authentificatoin
     *
     */
    authentificate(pNumber: string, callback: any) {

        logger.info("TeliaID Authenticate");

        let args = {};

        logger.info(pNumber);

        if (pNumber && pNumber !== "") {
            args = { personalNumber: pNumber };
        }

        logger.info(JSON.stringify(args));

        this.soapClient.Auth(args, function (err: any, result: any) {

            if (err) {
                // TODO: Error handler
                logger.error("Err:", err);

                callback(undefined, err);
            }
            else {
                callback(result.AuthResult, undefined);
            }
        });
    }
    collect(order: string, user: any, callback: any) {

        const self = this;

        const args = {orderRef: order};

        this.soapClient.Collect(args, function (err: any, result: any) {

            logger.info(self.soapClient.lastRequest);
            if (err) {
                callback(undefined, err);
            }
            else {
                result.user = user;
                callback(result, undefined);
            }
        });
    }

    sign(pNumber: string, visibleData: string, nonVisibleData: string , callback: any) {

        const self = this;

        const args = {personalNumber: pNumber, userVisibleData: visibleData, userNonVisibleData: nonVisibleData};

        this.soapClient.Sign(args, function (err: any, result: any) {

            logger.info(self.soapClient.lastRequest);
            if (err) {
                console.log("Error when processing soap", err);
                callback(undefined, err);
            } else {
                callback(result, undefined);

            }
        });
    }
}

